# ExpressionAnswerOptionsExt

Expression Script: make answer option text available inside survey by expression function.

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/ExpressionAnswerOptionsExt directory : `git clone https://gitlab.com/SondagesPro/ExpressionManager/ExpressionAnswerOptionsExt.git`

Plugin was tested on Version 5.2.2 , can work with version 4.0 and up.

## Usage

The plugin can be used on single choice question and array question , all question with realted answers editable by administrator.

The function getAnswerOptionText get 3 parameters

1. The question id or title
2. The answer code to get
3. The scale (for array dual scale question)

Some sample `getAnswerOptionText(self.qid,"A1")`, `getAnswerOptionText("Qcode","A1")` or `getAnswerOptionText(Qcode_SQ01.qid,"A1")`.

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/ExpressionManager/ExpressionAnswerOptionsExt).

## Home page & Copyright
- Copyright © 2021 Respondage <https://respondage.nl>
- Copyright © 2021 Denis Chenu <https://sondages.pro>
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
